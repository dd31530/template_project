#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
+----------------------------------------------------------------------------+
| messagesUtils:  toolbox for displaying formatted messages                  |
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | last change                                |
+---------+----------+----------+--------------------------------------------+
| 3.0     | 200504   | DD       | replace main() by autotest() for pytest    |
+---------+----------+----------+--------------------------------------------+
"""

VERSION = 'v3.0_200504'

"""
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | full history                               |
+---------+----------+----------+--------------------------------------------+
| 2.1     | 200312   | DD       | new history with current version on top    |
| 2.0     | 200206   | DD       | pycharm analysis                           |
| 1.1     | 190220   | DD       | adding status to footer                    |
| 1.0     | 151214   | DD       | creation                                   |
+---------+----------+----------+--------------------------------------------+
"""

##############################################################################
# external modules
##############################################################################


import sys
import inspect
import ast


##############################################################################
# local modules
##############################################################################


##############################################################################
# functions
##############################################################################


def header():
    """ displays current module docstring """
    f = inspect.stack()[1][1]
    m = ast.parse(''.join(open(f)))
    print("\n%s\n" % ast.get_docstring(m))


def banner(mess, code=0):
    """ displays banner message """
    sep = '\n+'+76*'-'+'+\n'
    mess = '| ' + mess + (75-len(mess))*' ' + '|'
    if code != 0:
        note = "please check logs for warnings and errors!"
        note = '\n| ' + note + (75-len(note))*' ' + '|'
        mess = mess + note
    print("%s%s%s" % (sep, mess, sep))


def footer(code=0):
    """ displays current module execution ending """
    banner("end of %s with code %s" % (sys.argv[0], code))
    return code


def autotest(error=False):
    from logUtils import Logger
    _rc = 1
    header()
    try:
        if error:
            raise RuntimeError
    except RuntimeError:
        banner("Autotest: error simulation", 255)
    else:
        Logger.logr.info("Autotest: header() ( ^^^^ above ^^^^ )")
        banner("Autotest...")
        Logger.logr.info("Autotest: footer() ( vvvv below vvvv )")
        _rc = 0
    finally:
        return footer(_rc)


##############################################################################
# main
##############################################################################


if __name__ == '__main__':
    sys.exit(autotest())


##############################################################################
# eof
##############################################################################