#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
+----------------------------------------------------------------------------+
| chronoUtils:  these are my own time count utilities                        |
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | last change                                |
+---------+----------+----------+--------------------------------------------+
| 3.0     | 200504   | DD       | replace main() by autotest() for pytest    |
+---------+----------+----------+--------------------------------------------+
"""

VERSION = 'v3.0_200504'

"""
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | full history                               |
+---------+----------+----------+--------------------------------------------+
| 2.2     | 200422   | DD       | GitLab/pycharm synchronization             |
| 2.1     | 200312   | DD       | new history with current version on top    |
| 2.0     | 200206   | DD       | pycharm analysis                           |
| 1.1     | 151215   | DD       | messagesUtils usage                        |
| 1.0     | 121119   | DD       | creation                                   |
+---------+----------+----------+--------------------------------------------+
"""

##############################################################################
# external modules
##############################################################################


import time as t


##############################################################################
# local modules
##############################################################################


##############################################################################
# classes
##############################################################################


class StartChrono:
    """ provide chronograph functionalities """
    def __init__(self):
        """ start chronograph """
        self._start = t.time()
        self._ref = self._start
        self._partial = t.time()

    def partial(self):
        """ returns partial elapsed time """
        self._partial = t.time()
        partial = self._partial - self._ref
        self._ref = self._partial
        return "partial elapsed time: %.2f s" % partial

    def stop(self):
        """ returns total elapsed time """
        total = t.time() - self._start
        return "total elapsed time: %.2f s" % total


##############################################################################
# functions
##############################################################################


def autotest(error=False):
    from logUtils import Logger
    _rc = 1
    try:
        if error:
            raise RuntimeError
        chronotest = StartChrono()
        t.sleep(1)
        Logger.logr.info(chronotest.partial())
        t.sleep(1)
        Logger.logr.info(chronotest.partial())
        t.sleep(1)
        Logger.logr.info(chronotest.partial())
        Logger.logr.info(chronotest.stop())
    except RuntimeError:
        pass
    else:
        _rc = 0
    finally:
        return _rc


##############################################################################
# main
##############################################################################


if __name__ == '__main__':
    from messagesUtils import header, banner, footer
    header()
    banner('Autotest...')
    rc = autotest()
    footer(rc)


##############################################################################
# eof
##############################################################################