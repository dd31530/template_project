# python template project
# Creation Date: 2015/12/15

the goal is to create the tree structure of a project

howto:

 1) local clone
 2) rename <template_project> to <your_new_project_name>
 3) edit <your_new_project_name>/cfg/log.conf and change <template.log> to <project_name.log>
 4) launch <your_new_project_name>/bin/<any_py_file>
 5) check <your_new_project_name>/log/<project_name.log>
 6) enjoy...
