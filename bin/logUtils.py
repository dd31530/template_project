#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
"""
+----------------------------------------------------------------------------+
| logUtils:  toolbox for logging functionality                               |
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | last change                                |
+---------+----------+----------+--------------------------------------------+
| 3.0     | 200504   | DD       | replace main() by autotest() for pytest    |
+---------+----------+----------+--------------------------------------------+
"""

VERSION = 'v3.0_200504'

"""
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | full history                               |
+---------+----------+----------+--------------------------------------------+
| 2.1     | 200312   | DD       | new history with current version on top    |
| 2.0     | 200206   | DD       | pycharm analysis                           |
| 1.1     | 190225   | DD       | adding setLevel() method                   |
| 1.0     | 151217   | DD       | creation                                   |
+---------+----------+----------+--------------------------------------------+
"""

##############################################################################
# external modules
##############################################################################


import os
import logging.config

##############################################################################
# local modules
##############################################################################


from pathUtils import CurrentPath


##############################################################################
# functions
##############################################################################


def singleton(cls):
    instances = dict()

    def get_instance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return get_instance()


##############################################################################
# classes
##############################################################################


@singleton
class Logger(object):
    def __init__(self):
        cswd = CurrentPath()
        os.environ["PROJECT_LOG_PATH"] = cswd.log()
        config = cswd.cfg('log.conf')
        logging.config.fileConfig(config)
        self.logr = logging.getLogger('root')

    @staticmethod
    def setLevel(level=20):
        logging.getLogger().setLevel(level)


def autotest(error=False):
    _rc = 1
    try:
        if error:
            raise RuntimeError
    except RuntimeError:
        pass
    else:
        Logger.setLevel()
        Logger.logr.debug('this is an autotest for debug message')
        Logger.logr.info('this is an autotest for info message')
        Logger.logr.warning('this is an autotest for warning message')
        Logger.logr.error('this is an autotest for error message')
        Logger.logr.critical('this is an autotest for critical message')
        _rc = 0
    finally:
        return _rc


##############################################################################
# main
##############################################################################


if __name__ == '__main__':
    from messagesUtils import header, banner, footer

    header()
    banner('Autotest...')
    rc = autotest()
    footer(rc)

##############################################################################
# eof
##############################################################################