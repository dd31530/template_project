#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
+----------------------------------------------------------------------------+
| pathUtils:  toolbox for pathname manipulation                              |
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | last change                                |
+---------+----------+----------+--------------------------------------------+
| 3.0     | 200504   | DD       | replace main() by autotest() for pytest    |
+---------+----------+----------+--------------------------------------------+
"""

VERSION = 'v3.0_200504'

"""
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | full history                               |
+---------+----------+----------+--------------------------------------------+
| 2.1     | 200312   | DD       | new history with current version on top    |
| 2.0     | 200206   | DD       | pycharm analysis                           |
| 1.2     | 151215   | DD       | messagesUtils usage                        |
| 1.1     | 121205   | DD       | replace unique default argument            |
|         |          |          | (filename='') by tuple (*args)             |
|         |          |          | in currentPath class methods               |
| 1.0     | 121116   | DD       | creation                                   |
+---------+----------+----------+--------------------------------------------+
"""

##############################################################################
# external modules
##############################################################################

import os

##############################################################################
# classes
##############################################################################


class CurrentPath(object):
    """ provide methods for getting project absolute pathname. """
    def __init__(self):
        self._bin = os.path.dirname(os.path.realpath(__file__))
        self._cfg = self._bin.replace('bin', 'cfg')
        self._inp = self._bin.replace('bin', 'input')
        self._out = self._bin.replace('bin', 'output')
        self._tmp = self._bin.replace('bin', 'tmp')
        self._log = self._bin.replace('bin', 'log')

    def bin(self, *args):
        """ returns current project 'bin' absolute path. """
        return os.path.join(self._bin, *args)
        
    def cfg(self, *args):
        """ returns current project 'cfg' absolute path. """
        return os.path.join(self._cfg, *args)
        
    def inp(self, *args):
        """ returns current project 'input' absolute path. """
        return os.path.join(self._inp, *args)
        
    def out(self, *args):
        """ returns current project 'output' absolute path. """
        return os.path.join(self._out, *args)
        
    def tmp(self, *args):
        """ returns current project 'tmp' absolute path. """
        return os.path.join(self._tmp, *args)
        
    def log(self, *args):
        """ returns current project 'log' absolute path. """
        return os.path.join(self._log, *args)

    @staticmethod
    def isFile(filepath):
        """ returns true if file exists. """
        if os.path.isfile(filepath):
            return True
        else:
            return False

    @staticmethod
    def isDir(dirpath):
        """ returns true if dir exists. """
        if os.path.isdir(dirpath):
            return True
        else:
            return False


##############################################################################
# main
##############################################################################

if __name__ == '__main__':
    from logUtils import Logger
    from messagesUtils import header, banner, footer
    header()
    banner("Auto test...")
    cwd = CurrentPath()
    config_file = cwd.cfg('log.conf')
    if cwd.isFile(config_file):
        Logger.logr.info("file %s exists!" % config_file)
    else:
        Logger.logr.warning("file %s doesn't exist!" % config_file)
    if cwd.isDir(cwd.inp()):
        Logger.logr.info("le repertoire %s exists!" % cwd.inp())
    else:
        Logger.logr.warning("le repertoire %s doesn't exist!" % cwd.inp())
    output_file = cwd.out('result')
    if not cwd.isFile(output_file):
        Logger.logr.warning("file %s doesn't exist!" % output_file)
    footer()


##############################################################################
# eof
##############################################################################